import { epic$ } from 'store/rootEpic';
import { delay, map, tap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { Observable } from 'rxjs';
import { Action } from 'redux';

export const PING_TOKEN = 'PING';
export const PONG_TOKEN = 'PONG';
export const NONE_TOKEN = 'NONE';

const actions = {
  ping: () => ({ type: PING_TOKEN }),
  pong: () => ({ type: PONG_TOKEN }),
  none: () => ({ type: NONE_TOKEN }),
};

const pingEpic = (action$): Observable<Action> =>
  action$.pipe(
    ofType('PING'),
    delay(1000),
    tap(x => console.log('ping', x)),
    map(actions.pong),
  );

const pongEpic = (action$): Observable<Action> =>
  action$.pipe(
    ofType('PONG'),
    delay(1000),
    tap(x => console.log('pong', x)),
    map(actions.none),
  );

epic$.next(pingEpic);
epic$.next(pongEpic);

export const usePingPongEpic = () => ({ actions });
