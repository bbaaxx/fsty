import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { usePingPongEpic } from './epic';
import { Helmet } from 'react-helmet-async';
import { DahhComponent } from './DahhComponent';

export function HomePage() {
  const dispatch = useDispatch();
  const epicActions = usePingPongEpic().actions;

  useEffect(() => {
    dispatch(epicActions.ping());
  });

  return (
    <>
      <Helmet>
        <title>Home Page</title>
        <meta name="description" content="A Boilerplate application homepage" />
      </Helmet>
      <DahhComponent />
    </>
  );
}
