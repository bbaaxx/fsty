import React, { useEffect } from 'react';
import { usePingPongEpic } from './epic';
import { useDispatch } from 'react-redux';

export function DahhComponent() {
  const dispatch = useDispatch();
  const epicActions = usePingPongEpic().actions;
  useEffect(() => {
    dispatch(epicActions.ping());
  });
  return (
    <>
      <span>Yolo</span>
    </>
  );
}
