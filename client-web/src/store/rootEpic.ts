import { BehaviorSubject, Observable } from 'rxjs';
import { combineEpics, ofType } from 'redux-observable';
import { tap, catchError, mergeMap, ignoreElements } from 'rxjs/operators';
import { Action } from 'redux';

const noneEpic = (action$): Observable<Action> =>
  action$.pipe(
    ofType('NONE'),
    tap(x => console.log('NONE', x)),
    ignoreElements(),
  );

export const epic$ = new BehaviorSubject(combineEpics(noneEpic));
export const rootEpic = (action$, state$, dependencies?): Observable<any> =>
  epic$.pipe(
    mergeMap(epic => epic(action$, state$, dependencies)),
    catchError((error, source) => {
      console.error(error);
      return source;
    }),
  );
