import { ajax } from 'rxjs/ajax';

export const getEpicsDependencies = () => ({ getJSON: ajax.getJSON });
