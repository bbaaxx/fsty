import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
// import { createInjectorsEnhancer } from 'redux-injectors';
// import createSagaMiddleware from 'redux-saga';

import { createEpicMiddleware } from 'redux-observable';
import { rootEpic } from './rootEpic';
import { getEpicsDependencies } from './di';

import { createReducer } from './reducers';

export function configureAppStore() {
  // const reduxSagaMonitorOptions = {};
  // const sagaMiddleware = createSagaMiddleware(reduxSagaMonitorOptions);
  // const { run: runSaga } = sagaMiddleware;
  const epicMiddleware = createEpicMiddleware({
    dependencies: getEpicsDependencies(),
  });

  // Create the store with saga middleware
  // const middlewares = [sagaMiddleware, epicMiddleware];
  const middlewares = [epicMiddleware];

  // const enhancers = [
  //   createInjectorsEnhancer({
  //     createReducer,
  //     runSaga,
  //   }),
  // ];

  const store = configureStore({
    reducer: createReducer(),
    middleware: [...getDefaultMiddleware(), ...middlewares],
    devTools: process.env.NODE_ENV !== 'production',
    // enhancers,
  });

  epicMiddleware.run(rootEpic);

  return store;
}
