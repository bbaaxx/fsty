import { r, HttpError, HttpStatus } from '@marblejs/core';
import { throwError } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

const notFound$ = r.pipe(
  r.matchPath('*'),
  r.matchType('*'),
  r.useEffect(req$ =>
    req$.pipe(
      mergeMap(() =>
        throwError(
          () => new HttpError('Route not found', HttpStatus.NOT_FOUND),
        ),
      ),
    ),
  ),
);
