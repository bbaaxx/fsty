import { HttpErrorEffect } from '@marblejs/core';
import { map, tap } from 'rxjs/operators';

interface MarbleError extends Error {
  status?: any;
  data?: any;
}

export const error$: HttpErrorEffect = req$ =>
  req$.pipe(
    tap(({ error }) => {
      console.log(
        'Dude this sucks:',
        Object.getOwnPropertyNames(error),
        Object.getPrototypeOf(error),
      );
    }),
    map(({ error }: { error: MarbleError }) => ({
      status: error.status,
      body: { status: error.status, message: error.message },
    })),
  );
