import { from, of } from 'rxjs';
import * as mongoose from 'mongoose';
import { r, HttpRequest } from '@marblejs/core';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';

const ObjectId = mongoose.Types.ObjectId;

interface reqWithPrams extends HttpRequest {
  params: {
    id: string;
  };
}

// Model => [ getAll$, getOne$, post$, put$, patch$, delete$ ]
export const generateBaseCrudEffect = DbModel => [
  // getAll$
  r.pipe(
    r.matchPath('/'),
    r.matchType('GET'),
    r.useEffect(req$ =>
      req$.pipe(
        mergeMap(() => from(DbModel.find())),
        tap(x => console.log('x:', x)),
        map(body => ({ body })),
      ),
    ),
  ),
  // getOne$
  r.pipe(
    r.matchPath('/:id'),
    r.matchType('GET'),
    r.useEffect(req$ =>
      req$.pipe(
        map((req: reqWithPrams) => req.params.id),
        mergeMap(id => from(DbModel.findOne({ _id: ObjectId(id) }))),
        catchError(error => {
          console.log(Object.getOwnPropertyNames(error));
          return of({ status: 502, body: error.message });
        }),
      ),
    ),
  ),
  // post$
  r.pipe(
    r.matchPath('/'),
    r.matchType('POST'),
    r.useEffect(req$ =>
      req$.pipe(
        map((req: reqWithPrams) => req.body),
        mergeMap(body =>
          from(
            DbModel.updateOne({ _id: ObjectId(body['_id']) }, body, {
              upsert: true,
            }),
          ),
        ),
        map(body => ({ body })),
      ),
    ),
  ),
  r.pipe(
    r.matchPath('/'),
    r.matchType('PUT'),
    r.useEffect(req$ =>
      req$.pipe(
        map((req: reqWithPrams) => req.body),
        mergeMap(body =>
          from(DbModel.replaceOne({ _id: ObjectId(body['_id']) }, body)),
        ),
        map(body => ({ body })),
      ),
    ),
  ),
  r.pipe(
    r.matchPath('/:id'),
    r.matchType('PATCH'),
    r.useEffect(req$ =>
      req$.pipe(
        map((req: reqWithPrams) => ({ id: req.params.id, body: req.body })),
        mergeMap(({ id, body }) =>
          from(DbModel.findOneAndUpdate({ _id: ObjectId(id) }, body)),
        ),
        map(body => ({ body })),
      ),
    ),
  ),
  r.pipe(
    r.matchPath('/:id'),
    r.matchType('DELETE'),
    r.useEffect(req$ =>
      req$.pipe(
        map((req: reqWithPrams) => req.params.id),
        mergeMap(id => from(DbModel.deleteOne({ _id: ObjectId(id) }))),
        map(body => ({ body })),
      ),
    ),
  ),
];
