import { combineRoutes } from '@marblejs/core';
import { Dog } from './models/Dog.model';
import { generateBaseCrudEffect } from '../common/genericCrudEffectBase';

export const dogs$ = combineRoutes('/dogs', generateBaseCrudEffect(Dog));

/*
curl -X GET localhost:3333/api/v1/dogs

curl -X POST -H "Content-Type: application/json" \
-d '{ "name": "bender", "breed": "beagle"}' \
localhost:3333/api/v1/dogs

curl -X GET localhost:3333/api/v1/dogs/60efa6055a9616055d82055c

curl -X POST -H "Content-Type: application/json" \
    -d '{ "_id":"60efa6055a9616055d82055c", "name": "fender", "breed": "beagle"}' \
    localhost:3333/api/v1/dogs

curl -X PUT -H "Content-Type: application/json" \
    -d '{ "_id":"60efa6055a9616055d82055c", "name": "lender", "breed": "beagle"}' \
    localhost:3333/api/v1/dogs

curl -X PATCH -H "Content-Type: application/json" \
    -d '{ "name": "bender", "breed": "beagle"}' \
    localhost:3333/api/v1/dogs/60efa6055a9616055d82055c

curl -X DELETE localhost:3333/api/v1/dogs/60efa6055a9616055d82055c

*/
