import { combineRoutes, useContext, r } from '@marblejs/core';
import { mapTo } from 'rxjs/operators';
import { dogs$ } from './dogs.effect';
import { Dependency2Token } from '../dependencies/diExample';

const indexGet$ = r.pipe(
  r.matchPath('/'),
  r.matchType('GET'),
  r.useEffect(req$ =>
    req$.pipe(mapTo({ body: { message: 'Hello, worldie!' } })),
  ),
);

const indexPost$ = r.pipe(
  r.matchPath('/'),
  r.matchType('POST'),
  r.useEffect((req$, ctx) =>
    req$.pipe(
      mapTo({ body: { message: useContext(Dependency2Token)(ctx.ask) } }),
    ),
  ),
);

export const api$ = combineRoutes('/api/v1', [indexGet$, indexPost$, dogs$]);
