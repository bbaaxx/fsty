import { bindEagerlyTo, bindTo } from '@marblejs/core';
import {
  Dependency1,
  Dependency2,
  Dependency1Token,
  Dependency2Token,
} from './diExample';
import { dbToken, dbDependency } from './database.dependency';

export const dependencies = [
  bindTo(Dependency1Token)(Dependency1),
  bindTo(Dependency2Token)(Dependency2),
  bindEagerlyTo(dbToken)(dbDependency),
];
