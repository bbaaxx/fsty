import { createContextToken, createReader } from '@marblejs/core';
import * as mongoose from 'mongoose';

const getConnectedDb = async () =>
  await new Promise((resolve, reject) => {
    const db = mongoose.connection;
    mongoose.connect(process.env.MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db.once('open', function () {
      console.log('Connection to DB successful');
      resolve(db);
    });
    db.on('error', reject);
  });

export const dbToken = createContextToken<string>('Database');
export const getdbToken = () => dbToken;
export const dbDependency = createReader(getConnectedDb);
