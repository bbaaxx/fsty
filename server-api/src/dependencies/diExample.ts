import { createContextToken, createReader, useContext } from '@marblejs/core';

export const Dependency1Token = createContextToken<string>('Dependency1');
export const Dependency2Token = createContextToken<string>('Dependency2');

export const getDependency1Token = () => Dependency1Token;
export const getDependency2Token = () => Dependency2Token;

// export const oldDependency2 = pipe(
//   reader,
//   R.map(ask =>
//     pipe(
//       ask(Dependency1Token),
//       O.map(v => v + ', dudly!'),
//       O.getOrElse(() => ''),
//     ),
//   ),
// );

export const Dependency1 = createReader(() => 'Hello');
export const Dependency2 = createReader(
  ask => useContext(getDependency1Token())(ask) + ', Dudly!',
);
