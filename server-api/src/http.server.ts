import { merge } from 'rxjs';
import { IO } from 'fp-ts/lib/IO';
import { map, tap } from 'rxjs/operators';
import {
  createServer,
  matchEvent,
  ServerEvent,
  HttpServerEffect,
} from '@marblejs/core';
import { listener } from './http.listener';
import { dependencies } from './dependencies';

const port = Number(process.env.PORT) !== NaN ? Number(process.env.PORT) : 4200;

const listening$: HttpServerEffect = event$ =>
  event$.pipe(
    matchEvent(ServerEvent.listening),
    map(event => event.payload),
    tap(({ port, host }) => console.log(`Listening @ http://${host}:${port}/`)),
  );

const server = createServer({
  port,
  listener,
  dependencies,
  event$: (...args) => merge(listening$(...args)),
});

export const marbleHttpServer: IO<void> = async () => await (await server)();
