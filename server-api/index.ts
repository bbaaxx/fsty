#!/usr/bin/env node
require('dotenv-safe').config();

import { marbleHttpServer } from './src/http.server';

marbleHttpServer();
