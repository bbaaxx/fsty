it("Renders a default message", () => {
  cy.visit("/");

  cy.get("#welcomePm").should("contain", "Hello");
});
